#ifndef LASER_H
#define LASER_H

#include "./include.h"

typedef struct Laser{
    double m_x;
    double m_y;
    double m_vy;
    SDL_Rect m_src;
}Laser;


#endif // LASER_H
